package readfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

public class ReadText {
	public static void main(final String[] args) throws IOException, SAXException, TikaException {
		String regex = "(2131|1800)[ -]*\\d{4}[ -]*\\d{4}[ -]*\\d{3}";
		// "(\\D|^)(?:2131|1800)(\\ |\\-|)[0-9]{4}(\\ |\\-|)[0-9]{4}(\\
		// |\\-|)[0-9]{3}(\\D|$)";

		// detecting the file type
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(
				// new File("C:\\Users\\minion\\Desktop\\test\\Card\\File
				// doc\\jcb2\\onguyen_jcb2.docx"));
				new File("C:\\Users\\minion\\Desktop\\test\\Card\\File ppt\\jcb2\\onguyen_jcb2.ppt"));
		ParseContext pcontext = new ParseContext();

		// Text document parser
		// TXTParser TexTParser = new TXTParser();
		AutoDetectParser parser = new AutoDetectParser();
		parser.parse(inputstream, handler, metadata, pcontext);
		String content = handler.toString();
		System.out.println("Contents of the document:" + content);
		System.out.println("------------------------------------------");
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(content);
		int i = 0;
		while (matcher.find()) {
			i++;
			System.out.println(matcher.group());
		}
		System.out.println("total find count: " + i);
		// System.out.println("Metadata of the document:");
		// String[] metadataNames = metadata.names();

		// for (String name : metadataNames) {
		// System.out.println(name + " : " + metadata.get(name));
		// }
	}
	// public static void main(final String[] args) throws IOException,
	// TikaException {
	//
	// //Assume sample.txt is in your current directory
	// File file = new File("C:\\Users\\minion\\Desktop\\test\\test.docx");
	//
	// //Instantiating Tika facade class
	// Tika tika = new Tika();
	// String filecontent = tika.parseToString(file);
	//// filecontent= filecontent.replace('\n', ' ');
	// System.out.println("Extracted Content: " + filecontent);
	// }
}
